The Cut Django App: Loan Calculator


================
thecut.loancalculator
================

To install this application (whilst in the project's activated virtualenv)::
    pip install git+ssh://git@git.thecut.net.au/thecut-loancalculator


Add the ``thecut.loancalculator`` to the project's ``INSTALLED_APPS`` setting::

    INSTALLED_APPS = (
        ...
        'thecut.loancalculator',
    )
