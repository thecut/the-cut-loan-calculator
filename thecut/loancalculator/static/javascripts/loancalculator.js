/* =========================================================================
*
* TheCut:
* Loan Calculator v0.01
*
* ==========================================================================
* Copyright Busara Perth Pty Ltd
* All rights reserved
* Author Elena Williams
* ==========================================================================
*
* Calculation Assumptions
*
* Assumptions are made about the number of weeks & fortnights in a year. A
* year is assumed to contain 52 weeks, 26 fortnights, 12 months.
*
* Your lender may calculate interest on a daily basis, but this calculator
* assumes all months are assumed to be of equal length.
*
* The calculator uses the unrounded repayment to calculate the amount of
* interest payable. Repayments are rounded to the nearer cent.
*
* ========================================================================== */


// Frequency per annum.
var FREQ = [1, 12, 26, 52];


function calcInterest(){
    if ((document.calc.loanVal.value == null || document.calc.loanVal.value.length == 0) ||
        (document.calc.terms.value == null || document.calc.terms.value.length == 0) ||
        (document.calc.rate.value == null || document.calc.rate.value.length == 0)) {
        // document.calc.pay.value = "Incomplete data";
    } else {
        var principal = parseInt(document.calc.loanVal.value);
        var term = parseInt(document.calc.termVal.value);
        var interest = document.calc.rate.value / 100;
        for(i=0;i<FREQ.length;i++){
            if (document.calc.freq[i].checked == true){
                var frequency = FREQ[parseInt(document.calc.freq[i].value)];
            }
        };
        if(document.getElementById('typeI').checked){
            var type = 'interestonly';
        } else {
            var type = 'principalinterest';
        }
        // annualInterest used by both the mainly calculation and the tabular data
        var annualInterest = principal*interest;

        /* ***
         * Main Calculation
         */

        if (type=='interestonly'){

            /* Interest Only */

            var valRepayment = annualInterest / frequency;
            var valTotalInterest = annualInterest * term;

        } else {

            /* Principal and Interest */

            var valRepayment = calcCompoundInt(principal, (term * frequency), (interest / frequency));
            var valTotalInterest = (valRepayment * (term * frequency)) - principal;
        }

        $('#repayment').html( formatDecDollar(valRepayment) );
        $('#totalInterest').html( formatDecDollar(valTotalInterest) );

        // valRepayment must be calculated before annualRepayment defined
        var annualRepayment = valRepayment * frequency;

        /* ***
         * Tabular Data
         *
         * data for use in graphs
         */

        // rough heading of data
        $('#resultsPeriod').html('y interest owing   principal interest<br>');

        var remainingPrincipal = principal;
        var remainingInterest = valTotalInterest;

        for(i=0;i<term+1;i++){
            // var valOngoingRepay = (calcCompoundInt(principal, i * frequency, interest / frequency)) * i/term;
            // var totalPaid = (valOngoingRepay * (term * frequency))
            // var totalInterest = totalPaid - principal

            var remainingTerm = term - i;

            if (type=='interestonly'){

                /* Interest Only */

                var remainingInterest = annualInterest * remainingTerm;

            } else {

                /* Principal and Interest */

                var remainingOwing = (valRepayment * (remainingTerm * frequency));
                var annualInterest = (remainingPrincipal) * interest

                if (i>0){
                    var remainingPrincipal = remainingPrincipal + annualInterest - annualRepayment;
                } else {
                    var remainingPrincipal = remainingPrincipal;
                }

                if (remainingPrincipal<0){
                    var remainingPrincipal = 0;
                }
                var remainingInterest = remainingOwing - remainingPrincipal;
           }

            // rough display of data
            $('#resultsPeriod').append(i +
                                       ', ' +
                                       formatRadix(parseInt(annualInterest)) +
                                       ', ' +
                                       formatRadix(parseInt(remainingOwing)) +
                                       ', ' +
                                       formatRadix(parseInt(remainingPrincipal)) +
                                       ', ' +
                                       formatRadix(parseInt(remainingInterest)) +
                                       '<br>');
        };
    };
};

function calcCompoundInt(principal, term, interest){
    return principal * interest / (1 - (Math.pow(1/(1 + interest), term)));
};



function formatRadix(num) {
    var val = num
    if (parseFloat(num)) {
        num = new String(num);
        var parts = num.split(".");
        parts[0] = parts[0].split("").reverse().join("").replace(/(\d{3})(?!$)/g, "$1,").split("").reverse().join("");
        val = parts.join(".");
    }
    return val;
};

function formatDollar(num){
    var val = formatRadix(num);
    return "$" + val;
};

function formatDecDollar(num){
    var val = formatRadix(num.toFixed(2));
    return "$" + val;
};

function formatStrip(val){
    var num = val.replace(/\,/g, '').replace(/\$/g, '');
    return parseInt(num);
};

jQuery(document).ready(function($) {

    $( "#loan, #terms, #rate, .freq, .type" ).change(function(){
        $( "#loanVal" ).val( formatStrip($( "#loan" ).val()));
        $( "#rateVal" ).val( parseFloat($( "#rate" ).val()));
        calcInterest();
    });

    $( "#loanSlider" ).slider({
        value:300000,
        min: 300000,
        max: 1000000,
        step: 25000,
        slide: function( event, ui ) {
            $( "#loan" ).val( formatDollar(ui.value) );
            $( "#loanVal" ).val( ui.value );
            calcInterest();
        }
    });
    $( "#loan" ).val( formatDollar($( "#loanSlider" ).slider( "value" )) );
    $( "#loanVal" ).val( $( "#loanSlider" ).slider( "value" ) );

    $( "#termSlider" ).slider({
        value: 25,
        min: 3,
        max: 30,
        step: 1,
        slide: function( event, ui ) {
            $( "#terms" ).val( ui.value );
            calcInterest();
        }
    });
    $( "#terms" ).val( $( "#termSlider" ).slider( "value" ) );

    $( "#rateSlider" ).slider({
        value: 6.25,
        min: 3,
        max: 20.00,
        step: 0.25,
        slide: function( event, ui ) {
            $( "#rate" ).val( (ui.value).toFixed(2) );
            $( "#rateVal" ).val( ui.value );
            calcInterest();
        }
    });
    $( "#rate" ).val( ($( "#rateSlider" ).slider( "value" )).toFixed(2) );
    // second field exists in case there are further decimal places. It could happen.
    $( "#rateVal" ).val( $( "#rateSlider" ).slider( "value" ) );

    // initialis calculation on load
    calcInterest();

});
